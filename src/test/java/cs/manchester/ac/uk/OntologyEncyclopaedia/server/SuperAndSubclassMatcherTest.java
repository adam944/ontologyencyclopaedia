package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class SuperAndSubclassMatcherTest {
	
	@Test
	public void testMatcher () {
		List<String> superClassURIs = new ArrayList<String> ();
		List<String> subClassURIs = new ArrayList<String> ();
		
		String super1 = "http://dummy.com/test/superclass1";
		String super2 = "http://dummy.com/test/superclass2";
		superClassURIs.add(super1);
		superClassURIs.add(super2);
		String sub1 = "http://dummy.com/test/subclass1";
		String sub2 = "http://dummy.com/test/subclass2";
		subClassURIs.add(sub1);
		subClassURIs.add(sub2);
		
		EncyclopaediaEntry entry = new EncyclopaediaEntry("http://www.dummy.com/test/aURI");
		entry.setSuperClassURIs(superClassURIs);
		entry.setSubClassURIs(subClassURIs);
		entry.setName("dummyName");
		
		Map <String, EncyclopaediaEntry> classesWithProperties = new HashMap <String, EncyclopaediaEntry> ();
		classesWithProperties.put("http://www.dummy.com/test/aURI", entry);
		classesWithProperties.put(super1, entry);
		classesWithProperties.put(super2, entry);
		classesWithProperties.put(sub1, entry);
		classesWithProperties.put(sub2, entry);
		
		SuperAndSubclassMatcher.findSuperAndSubClasses(classesWithProperties);
		
		List <SuperOrSubClass> superClasses = entry.getSuperClasses();
		List <SuperOrSubClass> subClasses = entry.getSubClasses();
		
		assertEquals ("http://dummy.com/test/superclass1", superClasses.get(0).getUri());
		assertEquals ("http://dummy.com/test/superclass2", superClasses.get(1).getUri());
		assertEquals ("http://dummy.com/test/subclass1", subClasses.get(0).getUri());
		assertEquals ("http://dummy.com/test/subclass2", subClasses.get(1).getUri());
	}
}
