package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Multimap;

public class OntologyExtractorTest {

	private OntologyExtractor testObj;
	private File ontology;
	
//	private OntologyLoader loader;

	@Before
	public void setUp () {
		testObj = OntologyExtractor.getInstance();
		OntologyLoader.getInstance().removeAllOntologies();
	}

	@Test
	public void testGetNonObsoleteClasses() {
		ontology = new File(System.getProperty("user.dir") + "/src/test/resources/goOneClass.owl");
		OntologyLoader.getInstance().loadOntology(ontology);
		testObj.initialise();
		Multimap <OWLOntology, OWLClass> classMap = testObj.getNonObsoleteClasses();
		Set <OWLOntology> keySet = classMap.keySet();
		Iterator <OWLOntology> keyIterator = keySet.iterator();
	    while (keyIterator.hasNext() ) {
	    	OWLOntology key = keyIterator.next();
	        List <OWLClass> values = (List<OWLClass>) classMap.get( key );
	        assertEquals ("<http://purl.obolibrary.org/obo/GO_2001310>", values.get(0).toString());
	    }
	}

	@Test
	public void testGetClassesWithDefinitions() {
		ontology = new File(System.getProperty("user.dir") + "/src/test/resources/goOneClass.owl");
		OntologyLoader.getInstance().loadOntology(ontology);
		testObj.initialise();
		Multimap <OWLOntology, OWLClass> classMap = testObj.getNonObsoleteClasses();
		Map <String, EncyclopaediaEntry> resultClassMap = testObj.getClassesWithProperties(classMap);
		EncyclopaediaEntry entry = null;;
		for (String key : resultClassMap.keySet()) {
			entry = resultClassMap.get(key);
		}
		assertEquals ("this is a dummy class name", entry.getName());
		assertEquals ("this is a dummy definition", entry.getDefinition());
	}

	@Test
	public void testGetNoOutputForObsoleteClass () {
		ontology = new File(System.getProperty("user.dir") + "/src/test/resources/goOneObsoleteClass.owl");
		OntologyLoader.getInstance().loadOntology(ontology);
		testObj.initialise();

		// This should be empty
		Multimap <OWLOntology, OWLClass> classMap = testObj.getNonObsoleteClasses();

		assertEquals(0, classMap.size());
	}
}
