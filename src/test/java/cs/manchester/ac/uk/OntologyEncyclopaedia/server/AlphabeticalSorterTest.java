package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class AlphabeticalSorterTest {

	private AlphabeticalSorter testObj;

	@Before
	public void setUp () {
		Map <String, EncyclopaediaEntry> classList = new HashMap <String, EncyclopaediaEntry> ();
		EncyclopaediaEntry class1 = new EncyclopaediaEntry("http://purl.obolibrary.org/obo/GO_0000008");
		class1.setName("I am class 1");
		classList.put("class1URI", class1);
		
		EncyclopaediaEntry class2 = new EncyclopaediaEntry("http://purl.obolibrary.org/obo/GO_0000009");
		class2.setName("and this is class 2");
		classList.put("class2URI", class2);
		
		EncyclopaediaEntry class3 = new EncyclopaediaEntry("http://purl.obolibrary.org/obo/GO_0000006");
		class3.setName("Hi My NAME is class3");
		classList.put("class3URI", class3);
		
		EncyclopaediaEntry class4 = new EncyclopaediaEntry("http://purl.obolibrary.org/obo/GO_0000010");
		class4.setName("(-je m'appelles class4 and I have special starting characters)");
		classList.put("class4URI", class4);
		
		EncyclopaediaEntry class5 = new EncyclopaediaEntry("http://purl.obolibrary.org/obo/GO_0000010");
		class5.setName("@");
		classList.put("class5URI", class5);
		
		testObj = AlphabeticalSorter.getInstance(classList);
	}

	@Test
	public void testSortIntoAlphabeticalBuckets () {
		testObj.sortIntoAlphabeticalBuckets();
		List <String> classNameList = new ArrayList<String> ();
		for (char letter = 'a'; letter <= 'z'; letter++) {
			List <EncyclopaediaEntry> resultList = testObj.getBucketWithLetter(letter);
			if (resultList != null && resultList.size() != 0) {
				for (int index = 0; index < resultList.size(); index ++) {
					classNameList.add(resultList.get(index).getName());
				}
			}
		}
		assertEquals ("and this is class 2", classNameList.get(0));
		assertEquals("Hi My NAME is class3", classNameList.get(1));
		assertEquals ("I am class 1", classNameList.get(2));
		assertEquals ("(-je m'appelles class4 and I have special starting characters)", classNameList.get(3));
	}
}
