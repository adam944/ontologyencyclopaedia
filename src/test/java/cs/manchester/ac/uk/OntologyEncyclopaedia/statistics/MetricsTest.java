package cs.manchester.ac.uk.OntologyEncyclopaedia.statistics;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyLoader;

public class MetricsTest {
	
	private OntologyLoader loader = OntologyLoader.getInstance();
	
	@Before
	public void setUp () {
		loader.removeAllOntologies();
		loader.loadOntology(new File(System.getProperty("user.dir") + "/src/test/resources/goOneClass.owl"));
		Metrics.initialise();
	}
	
	@Test
	public void testGetTotalNumberOfClasses () {
		int total = Metrics.getTotalNumberOfClasses();
		assertEquals (1, total);
	}
	
	@Test
	public void testGetNumberOfObsoleteClasses () {
		int total = Metrics.getNumberOfObsoleteClasses();
		assertEquals(0, total);
	}
	
	@Test
	public void testGetAvergeNumberOfSynonyms () {
		double average = Metrics.getAvergeNumberOfSynonyms();
		assertEquals(1.5, average, 0);
	}
	
}
