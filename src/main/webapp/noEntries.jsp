<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>

<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/style.css">
<link rel="shortcut icon" href="favicon.ico" />
<title>Search results</title>
</head>
<body style="padding-top:50px;">

<h1 align="center">Sorry, there are no entries on this page</h1>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">Ontology Encyclopaedia</a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
				<!-- Insert links on navbar here -->
					<div class="collapse navbar-collapse">
						<shiro:guest>
							<form class="navbar-form navbar-right" action="login.jsp">
								<button type="submit" class="btn btn-primary">Admin Login</button>
							</form>
						</shiro:guest>
						<shiro:user>
							<form class="navbar-form navbar-right" action="Admin" method="GET">
								<button type="submit" class="btn btn-primary">Admin Panel</button>
							</form>
						</shiro:user>
						
						<form class="navbar-form navbar-left navbar-input-group" role="search" action=Search method="post">
  							<div class="form-group">
    							<input type="text" class="form-control" style="width: 300px" name="term" placeholder="Search the Encyclopaedia...">
  							</div>
  							<button type="submit" class="btn btn-success">Search</button>
						</form>
					</div>
					<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
	</nav>
	
	<div id="v_nav_bar">
	<a class="${a_active}" href="Encyclopaedia?letter=a">a</a> 
				<a class="${b_active}" href="Encyclopaedia?letter=b">b</a> 
				<a class="${c_active}"	href="Encyclopaedia?letter=c">c</a> 
				<a class="${d_active}"	href="Encyclopaedia?letter=d">d</a>
				<a class="${e_active}"	href="Encyclopaedia?letter=e">e</a> 
				<a class="${f_active}"	href="Encyclopaedia?letter=f">f</a> 
				<a class="${g_active}"	href="Encyclopaedia?letter=g">g</a> 
				<a class="${h_active}"	href="Encyclopaedia?letter=h">h</a> 
				<a class="${i_active}"	href="Encyclopaedia?letter=i">i</a> 
				<a class="${j_active}"	href="Encyclopaedia?letter=j">j</a> 
				<a class="${k_active}"	href="Encyclopaedia?letter=k">k</a> 
				<a class="${l_active}"	href="Encyclopaedia?letter=l">l</a> 
				<a class="${m_active}"	href="Encyclopaedia?letter=m">m</a> 
				<a class="${n_active}"	href="Encyclopaedia?letter=n">n</a> 
				<a class="${o_active}"	href="Encyclopaedia?letter=o">o</a> 
				<a class="${p_active}"	href="Encyclopaedia?letter=p">p</a> 
				<a class="${q_active}"	href="Encyclopaedia?letter=q">q</a> 
				<a class="${r_active}"	href="Encyclopaedia?letter=r">r</a> 
				<a class="${s_active}"	href="Encyclopaedia?letter=s">s</a> 
				<a class="${t_active}"	href="Encyclopaedia?letter=t">t</a> 
				<a class="${u_active}"	href="Encyclopaedia?letter=u">u</a> 
				<a class="${v_active}"	href="Encyclopaedia?letter=v">v</a> 
				<a class="${w_active}"	href="Encyclopaedia?letter=w">w</a> 
				<a class="${x_active}"	href="Encyclopaedia?letter=x">x</a> 
				<a class="${y_active}"	href="Encyclopaedia?letter=y">y</a> 
				<a class="${z_active}"	href="Encyclopaedia?letter=z">z</a>
</div>


</body>
</html>