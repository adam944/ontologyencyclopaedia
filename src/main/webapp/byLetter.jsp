<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="javax.servlet.*" %>
<!DOCTYPE html>
<html>
<head>
<title>OBOPedia</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/style.css">
<link rel="shortcut icon" href="favicon.ico" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Cardo:400italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:100,400,300italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/MooTools-Core-1.5.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/menu.js"> </script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
<script>
	$(document).ready(function() {
		function lazyLoadTop() {
				var link = $("#loadMoreTop")[0]
				link.click()
		}
		function lazyLoadBottom() {
			var url = window.location.href;
			
				var link = $("#loadMoreBottom")[0]
				link.click()
		}

		var url = window.location.href;
		var loadMoreParameter = 'loadMore';
		if(url.indexOf('?' + loadMoreParameter + '=top') != -1) {
			lazyLoadTop()
		} 
		else if(url.indexOf('?' + loadMoreParameter + '=bottom') != -1) {
			lazyLoadBottom()
		}
	})
</script>
<script type="text/javascript">
$(document).ready(function() {
  
  $("[id^=ratingForm]").on('submit', function(e) {
    e.preventDefault();
    var btn = $(document.activeElement);
    var dataString = "rating=" + btn.val() + "&uri=" + $(this).find('.uri').val();

  	var rating_value = $(this).find(".ratingValue")
    $.ajax({
      url: 'Encyclopaedia', 
      type: 'POST', 
      dataType: 'html', 
      data: dataString, 
      success: function(data) {
        	rating_value.html("Rating: " + data)
      },
      error: function(e) {
        console.log(e)
      }
    });
  });
});
</script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">OBOPedia</a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
				<!-- Insert links on navbar here -->
					<div class="collapse navbar-collapse">
						<shiro:guest>
							<form class="navbar-form navbar-right" action="login.jsp">
								<button type="submit" class="btn btn-primary">Admin Login</button>
							</form>
						</shiro:guest>
						<shiro:user>
							<form class="navbar-form navbar-right" action="Admin" method="GET">
								<button type="submit" class="btn btn-primary">Admin Panel</button>
							</form>
						</shiro:user>
						
						<form class="navbar-form navbar-left navbar-input-group" role="search" action=Search method="post">
  							<div class="form-group">
    							<input type="text" class="form-control" style="width: 300px" name="term" placeholder="Search the Encyclopaedia...">
  							</div>
  							<button type="submit" class="btn btn-success">Search</button>
						</form>
					</div>
					<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
	</nav>

<div id="v_nav_bar">
	<a class="${a_active}" href="Encyclopaedia?letter=a">a</a> 
				<a class="${b_active}" href="Encyclopaedia?letter=b">b</a> 
				<a class="${c_active}"	href="Encyclopaedia?letter=c">c</a> 
				<a class="${d_active}"	href="Encyclopaedia?letter=d">d</a>
				<a class="${e_active}"	href="Encyclopaedia?letter=e">e</a> 
				<a class="${f_active}"	href="Encyclopaedia?letter=f">f</a> 
				<a class="${g_active}"	href="Encyclopaedia?letter=g">g</a> 
				<a class="${h_active}"	href="Encyclopaedia?letter=h">h</a> 
				<a class="${i_active}"	href="Encyclopaedia?letter=i">i</a> 
				<a class="${j_active}"	href="Encyclopaedia?letter=j">j</a> 
				<a class="${k_active}"	href="Encyclopaedia?letter=k">k</a> 
				<a class="${l_active}"	href="Encyclopaedia?letter=l">l</a> 
				<a class="${m_active}"	href="Encyclopaedia?letter=m">m</a> 
				<a class="${n_active}"	href="Encyclopaedia?letter=n">n</a> 
				<a class="${o_active}"	href="Encyclopaedia?letter=o">o</a> 
				<a class="${p_active}"	href="Encyclopaedia?letter=p">p</a> 
				<a class="${q_active}"	href="Encyclopaedia?letter=q">q</a> 
				<a class="${r_active}"	href="Encyclopaedia?letter=r">r</a> 
				<a class="${s_active}"	href="Encyclopaedia?letter=s">s</a> 
				<a class="${t_active}"	href="Encyclopaedia?letter=t">t</a> 
				<a class="${u_active}"	href="Encyclopaedia?letter=u">u</a> 
				<a class="${v_active}"	href="Encyclopaedia?letter=v">v</a> 
				<a class="${w_active}"	href="Encyclopaedia?letter=w">w</a> 
				<a class="${x_active}"	href="Encyclopaedia?letter=x">x</a> 
				<a class="${y_active}"	href="Encyclopaedia?letter=y">y</a> 
				<a class="${z_active}"	href="Encyclopaedia?letter=z">z</a>
</div>

<div class="content">
	<a id="loadMoreTop" href="#${startingAndEndingValues[0]}"  style="display:none"></a>
	<form class="text-center" action="Encyclopaedia" method="GET">
		<button type="submit" name="loadMore" value="top" class="btn btn-primary">Load More...</button>
	</form>
	<div class="class-table">
		<table class="table table-striped" style="table-layout: fixed;">
			<tbody>
				<c:forEach items="${classesWithProperties}" var="entry">
					<tr>
						<td width='30%'>
							<span class="className" id="${entry.getTag()}">${entry.getName()}</span><br><br>
							
							<span class="classSynonym">Exact Synonym(s):</span>
							<span class="classSynonymList">
								<c:choose>
									<c:when test="${entry.getExactSynonyms().get(0) == \"-\"}">-<br></c:when>
									<c:otherwise><div class="scrollable"><c:forEach items="${entry.getExactSynonyms()}" var="synonym"><br>${synonym.toString()}</c:forEach></div><br></c:otherwise>
								</c:choose>
							</span>
							
							<br>
							
							<span class="classSynonym">Broad Synonym(s):</span> 
							<span class="classSynonymList">
								<c:choose>
									<c:when test="${entry.getBroadSynonyms().get(0) == \"-\"}">-<br></c:when>
									<c:otherwise><div class="scrollable"> <c:forEach items="${entry.getBroadSynonyms()}" var="synonym"><br>${synonym.toString()}</c:forEach></div><br></c:otherwise>
								</c:choose> 
							</span>
							
							<br>
							
							<span class="classSynonym">Narrow Synonym(s):</span>
							<span class="classSynonymList">
								<c:choose>
									<c:when test="${entry.getNarrowSynonyms().get(0) == \"-\"}">-<br></c:when>
									<c:otherwise><div class="scrollable"><c:forEach items="${entry.getNarrowSynonyms()}" var="synonym"><br>${synonym.toString()}</c:forEach></div><br></c:otherwise>
								</c:choose>
							</span>	
							
							<br>
							
							<span class="classSynonym">Related Synonym(s):</span>
							<span class="classSynonymList">
								<c:choose>
									<c:when test="${entry.getRelatedSynonyms().get(0) == \"-\"}">-<br></c:when>
									<c:otherwise><div class="scrollable"><c:forEach items="${entry.getRelatedSynonyms()}" var="synonym"><br>${synonym.toString()}</c:forEach></div><br></c:otherwise>
								</c:choose> 
							</span>
							
						</td>
						<td width='50%' style="border-right:1px solid black">
							<span class="classDef"><i>
								<c:choose> 
									<c:when test="${entry.getDefinition() == \"-\"}">No definition found.</c:when>
									<c:otherwise>${entry.getDefinition()}</c:otherwise>
								</c:choose>
							</i></span>
							
							<hr style="background:#5cb85c; border:0; height:4px;" />
								<div class="sourceName">
									Source: <a target="_blank" href=${entry.getSourceOntologyURL()}>${entry.getSourceOntologyName()}</a>
								</div>
								<br>
								<form class="text-center" id="ratingForm${entry.getURI()}" action="" method="POST" name="ratingForm"> 
									<div class="ratingValue"> Rating: ${entry.getRating()} </div>
									Rate:
									<input type="hidden" class="uri" value="${entry.getURI()}">
									<button type="submit" class="btn btn-default btn-xs rating"  name="rating" value="1">
  										<span class="glyphicon glyphicon-star" aria-hidden="true"></span> 
									</button>
									<button type="submit" class="btn btn-default btn-xs rating"  name="rating" value="2">
  										<span class="glyphicon glyphicon-star" aria-hidden="true"></span> 
									</button>
									<button type="submit" class="btn btn-default btn-xs rating"  name="rating" value="3">
  										<span class="glyphicon glyphicon-star" aria-hidden="true"></span> 
									</button>
									<button type="submit" class="btn btn-default btn-xs rating"  name="rating" value="4">
  										<span class="glyphicon glyphicon-star" aria-hidden="true"></span> 
									</button>
									<button type="submit" class="btn btn-default btn-xs rating"  name="rating" value="5">
  										<span class="glyphicon glyphicon-star" aria-hidden="true"></span> 
									</button>
								</form>
							
						</td>
						
						<td align="center" style="word-wrap: break-word">
						<div class="scrollable">
							<font size="4">More General:</font>
							<br>
							<c:choose>
									<c:when test="${entry.getSuperClasses().isEmpty()}">-<br></c:when>
									<c:otherwise><c:forEach items="${entry.getSuperClasses()}" var="superclass" varStatus="loopStatus">
										<c:url value="/Encyclopaedia" var="url">
  											<c:param name="letter" value="${superclass.getStartingLetter()}" />
  											<c:param name="uri" value="${superclass.getUri()}" />
										</c:url>
										<c:set var="superTag" value="${fn:replace(superclass.getName(),' ', '-')}" />
										<a href="${url}#${superTag}">${superclass.getName()}</a><c:if test="${!loopStatus.isLast()}">, </c:if>
									</c:forEach><br></c:otherwise>
							</c:choose>
							</div>
							<br/>
							<div class="scrollable">
							<font size="4">More Specific:</font>
							<br/>
							<c:choose>
									<c:when test="${entry.getSubClasses().isEmpty()}">-<br></c:when>
									<c:otherwise><c:forEach items="${entry.getSubClasses()}" var="subclass" varStatus="loopStatus">
									<c:url value="/Encyclopaedia" var="url">
  											<c:param name="letter" value="${subclass.getStartingLetter()}" />
  											<c:param name="uri" value="${subclass.getUri()}" />
										</c:url>
										<c:set var="subTag" value="${fn:replace(subclass.getName(),' ', '-')}" />
										<a href="${url}#${subTag}">${subclass.getName()}</a><c:if test="${!loopStatus.isLast()}">, </c:if>
									</c:forEach><br></c:otherwise>
							</c:choose>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<a id="loadMoreBottom" href="#${startingAndEndingValues[1]}"  style="display:none"></a>
	<form class="text-center" action="Encyclopaedia" method="GET">
		<button type="submit" name="loadMore" value="bottom" class="btn btn-primary">Load More...</button>
	</form>
</div>  
</body>
</html>

