<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Encyclopaedia Admin Login</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css_and_fonts/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/css_and_fonts/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

  <body>

    <div class="container">

      <form class="form-signin" name="loginform" action="Admin?action=login" method="POST" accept-charset="UTF-8" role="form">
        <h2 class="form-signin-heading">Admin Login</h2>
        <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="">
        <input type="password" class="form-control" name="password" placeholder="Password" required="">
<!--         <label class="checkbox"> -->
<!--           <input type="checkbox" value="remember-me"> Remember me -->
<!--         </label> -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  

</body></html>