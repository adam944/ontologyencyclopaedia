<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>

<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/style.css">
<link rel="shortcut icon" href="favicon.ico" />
<title>Search results</title>
</head>
<body style="padding-top:50px;">

<h1 align="center">Results for <i>${term}</i>:</h1>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">OBOPedia</a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
				<!-- Insert links on navbar here -->
					<div class="collapse navbar-collapse">
						<shiro:guest>
							<form class="navbar-form navbar-right" action="login.jsp">
								<button type="submit" class="btn btn-primary">Admin Login</button>
							</form>
						</shiro:guest>
						<shiro:user>
							<form class="navbar-form navbar-right" action="Admin" method="GET">
								<button type="submit" class="btn btn-primary">Admin Panel</button>
							</form>
						</shiro:user>
						
						<form class="navbar-form navbar-left navbar-input-group" role="search" action=Search method="post">
  							<div class="form-group">
    							<input type="text" class="form-control" style="width: 300px" name="term" placeholder="Search the Encyclopaedia...">
  							</div>
  							<button type="submit" class="btn btn-success">Search</button>
						</form>
					</div>
					<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
	</nav>

<div class="background">
<div class="content">
	<table class="table table-striped">
	<thead>
		<tr>
			<th class="text-center">Result</th>
			<th class="text-center">Source Ontology</th>
		</tr>
	</thead>
		<tbody>
			<c:forEach items="${results}" var="entry">
				<tr>
					<td align="center">
						<c:url value="/Encyclopaedia" var="url">
  							<c:param name="letter" value="${entry.getStartingLetter()}" />
  							<c:param name="uri" value="${entry.getURI()}" />
						</c:url>
						<a href="${url}#${entry.getTag()}">${entry.getName()}</a>
					</td>
					<td align="center">
						${entry.getSourceOntologyName()}
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
</div>
</body>
</html>