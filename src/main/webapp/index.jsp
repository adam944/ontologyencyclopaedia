<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<html>
<head>
<meta charset="UTF-8">
<title>OBOPedia</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/style.css">
<link rel="shortcut icon" href="favicon.ico" />

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">OBOPedia</a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
				<!-- Insert links on navbar here -->
					<div class="collapse navbar-collapse">
						<shiro:guest>
							<form class="navbar-form navbar-right" action="login.jsp">
								<button type="submit" class="btn btn-primary">Admin Login</button>
							</form>
						</shiro:guest>
						<shiro:user>
							<form class="navbar-form navbar-right" action="Admin" method="GET">
								<button type="submit" class="btn btn-primary">Admin Panel</button>
							</form>
						</shiro:user>
						
						<form class="navbar-form navbar-left navbar-input-group" role="search" action=Search method="post">
  							<div class="form-group">
    							<input type="text" class="form-control" style="width: 300px" name="term" placeholder="Search the Encyclopaedia...">
  							</div>
  							<button type="submit" class="btn btn-success">Search</button>
						</form>
					</div>
					<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
	</nav>

	<div class="jumbotron">
		<div class="container">
			<h1>Welcome to the OBOPedia!</h1>
			<p>This on-line encyclopaedia provides a navigable representation of the ontologies within the OBO Foundry, a concise biology ontology domain. 
			<br><br>
			Please choose a letter to browse the classes...</p>
			<p>
				<a  href="Encyclopaedia?letter=a">a</a> 
				<a  href="Encyclopaedia?letter=b">b</a> 
				<a	href="Encyclopaedia?letter=c">c</a> 
				<a	href="Encyclopaedia?letter=d">d</a>
				<a	href="Encyclopaedia?letter=e">e</a> 
				<a	href="Encyclopaedia?letter=f">f</a> 
				<a	href="Encyclopaedia?letter=g">g</a> 
				<a	href="Encyclopaedia?letter=h">h</a> 
				<a	href="Encyclopaedia?letter=i">i</a> 
				<a	href="Encyclopaedia?letter=j">j</a> 
				<a	href="Encyclopaedia?letter=k">k</a> 
				<a	href="Encyclopaedia?letter=l">l</a> 
				<a	href="Encyclopaedia?letter=m">m</a> 
				<a	href="Encyclopaedia?letter=n">n</a> 
				<a	href="Encyclopaedia?letter=o">o</a> 
				<a	href="Encyclopaedia?letter=p">p</a> 
				<a	href="Encyclopaedia?letter=q">q</a> 
				<a	href="Encyclopaedia?letter=r">r</a> 
				<a	href="Encyclopaedia?letter=s">s</a> 
				<a	href="Encyclopaedia?letter=t">t</a> 
				<a	href="Encyclopaedia?letter=u">u</a> 
				<a	href="Encyclopaedia?letter=v">v</a> 
				<a	href="Encyclopaedia?letter=w">w</a> 
				<a	href="Encyclopaedia?letter=x">x</a> 
				<a	href="Encyclopaedia?letter=y">y</a> 
				<a	href="Encyclopaedia?letter=z">z</a>
			</p>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>Super- and sublcasses</h2>
				<p>The super- and subclasses are now available for each entry for 
				the Encyclopaedia! Find them under the 'More General' / 'More Specific' section
				and click on them to find out more!</p>
				<!--           <p><a class="btn btn-default" href="#" role="button">View details »</a></p> -->
			</div>
			<div class="col-md-4">
				<h2>Blazing fast display of classes</h2>
				<p>The Encyclopaedia's performance has been massively
				enchanced so now it's much quicker to go to searched terms
				or load a full page of entries!</p>
				<!--           <p><a class="btn btn-default" href="#" role="button">View details »</a></p> -->
			</div>
			<div class="col-md-4">
				<h2>Feedback</h2>
				<p>The site now provides a wide range of
				functionality but surely it can be improved even further.
				Any recommendations? Drop an email!</p>
				<p>
					<a class="btn btn-primary btn-lg"
						href="mailto:adam.nogradi@student.manchester.ac.uk"
						" role="button">Send email �</a>
				</p>
			</div>
		</div>

		<hr>

		<footer>
			<div>� Adam Nogradi 2014
			<span class="pull-right"><a href="http://www.manchester.ac.uk" target="_blank"><img src="uom_logo.png" width="128px" height="54px"></img></a></span>
		</div>
		</footer>
	</div>


</body>
</body>
</html>