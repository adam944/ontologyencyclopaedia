<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css_and_fonts/style.css">

    <title>Admin Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/css_and_fonts/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/css_and_fonts/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css">

  
  </style>
  
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">OBOPedia</a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
				<!-- Insert links on navbar here -->
					<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=a">a</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=b">b</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=c">c</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=d">d</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=e">e</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=f">f</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=g">g</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=h">h</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=i">i</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=j">j</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=k">k</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=l">l</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=m">m</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=n">n</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=o">o</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=p">p</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=q">q</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=r">r</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=s">s</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=t">t</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=u">u</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=v">v</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=w">w</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=x">x</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=y">y</a></li>
					<li><a href="${pageContext.request.contextPath}/Encyclopaedia?letter=z">z</a></li>
				</ul>
					</div>
					<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
	</nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview</a></li>
            <li><a href="${pageContext.request.contextPath}/Admin?page=addRemove">Add / remove an ontology</a></li>
            <li><a href="${pageContext.request.contextPath}/Admin?page=update">Update ontologies</a></li>
          </ul>
          
          <ul class="nav nav-sidebar">
            <li><a href="${pageContext.request.contextPath}/logout">Log out</a></li>
          </ul>  
          
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
            		<div class="icon"><span class="glyphicon glyphicon-globe"></span></div>
              <h4>${noOfOntologies}</h4>
              <span class="text-muted">Ontologies</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
					<div class="icon"><span class="glyphicon glyphicon-th-list"></span></div>
              <h4>${noOfClasses}</h4>
              <span class="text-muted">Classes</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
            		<div class="icon"><span class="glyphicon glyphicon-link"></span></div>
              <h4>${averageSynonymsPerEntry}</h4>
              <span class="text-muted">Average synonyms per entry</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
            		<div class="icon"><span class="glyphicon glyphicon-remove-sign"></span></div>
              <h4>${obsoleteClasses}</h4>
              <span class="text-muted">Filtered out as obsolete</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>