package cs.manchester.ac.uk.OntologyEncyclopaedia.search;

import java.util.ArrayList;
import java.util.List;

import cs.manchester.ac.uk.OntologyEncyclopaedia.server.AlphabeticalSorter;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaEntry;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaException;

public class SearchResultsExtractor {
	private AlphabeticalSorter sorter = AlphabeticalSorter.getInstance();
	
	public List <EncyclopaediaEntry> getClassesAroundTag (EncyclopaediaEntry entry, int startingNumber, int endingNumber) {
		List <EncyclopaediaEntry> bucketWithEntry = sorter.getBucketWithLetter(entry.getTag().charAt(0));
		int classNumberInBucket = findClassInBucket (entry, bucketWithEntry);
		List <EncyclopaediaEntry> classesBefore = getClassesBefore(bucketWithEntry, classNumberInBucket, 10);
		List <EncyclopaediaEntry> classesAfter = getClassesAfter(bucketWithEntry, classNumberInBucket, 10);
		
		List <EncyclopaediaEntry> resultList = new ArrayList<EncyclopaediaEntry>();
		resultList.addAll(classesBefore);
		resultList.add(entry);
		resultList.addAll(classesAfter);
		
		return resultList;
	}
	
	private int findClassInBucket(EncyclopaediaEntry entry, List<EncyclopaediaEntry> bucketWithEntry) {
		for (int index = 0; index < bucketWithEntry.size();) {
			if (bucketWithEntry.get(index).getURI().equals(entry.getURI())) {
				return index;
			} else {
				throw new EncyclopaediaException("Class " + entry.getURI() + " has not been found...");
			}
		}
		return 0;
	}

	private List <EncyclopaediaEntry> getClassesBefore (List <EncyclopaediaEntry> bucket, int entryNumber, int numberOfClassesToReturn) {
		List <EncyclopaediaEntry> classesBefore = new ArrayList <EncyclopaediaEntry> ();
		
		for (int index = entryNumber - 1; index < numberOfClassesToReturn; index--) {
			classesBefore.add(bucket.get(index));
		}
		return classesBefore;
	}
	
	private List <EncyclopaediaEntry> getClassesAfter (List <EncyclopaediaEntry> bucket, int entryNumber, int numberOfClassesToReturn) {
		List <EncyclopaediaEntry> classesAfter = new ArrayList <EncyclopaediaEntry> ();
		
		for (int index = entryNumber + 1; index < numberOfClassesToReturn; index++) {
			classesAfter.add(bucket.get(index));
		}
		return classesAfter;
	}
}
