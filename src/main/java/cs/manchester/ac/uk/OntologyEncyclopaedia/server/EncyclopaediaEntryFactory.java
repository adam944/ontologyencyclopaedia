package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAnnotationValue;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;


public class EncyclopaediaEntryFactory {
	public EncyclopaediaEntry getNewEntry (OWLOntology ontology, OWLClass cl) {
		EncyclopaediaEntry currentEntry = new EncyclopaediaEntry(cl.toString());
		currentEntry.setName(getAnnotationPropertyFromClass(ontology, cl, AnnotationVocabulary.rdfsLabel).get(0));
		currentEntry.setDefinition(getAnnotationPropertyFromClass(ontology, cl, AnnotationVocabulary.hasDefinition).get(0));
		currentEntry.setExactSynonyms(getAnnotationPropertyFromClass(ontology, cl, AnnotationVocabulary.hasExactSynonym));
		currentEntry.setBroadSynonyms(getAnnotationPropertyFromClass(ontology, cl, AnnotationVocabulary.hasBroadSynonym));
		currentEntry.setNarrowSynonyms(getAnnotationPropertyFromClass(ontology, cl, AnnotationVocabulary.hasNarrowSynonym));
		currentEntry.setRelatedSynonyms(getAnnotationPropertyFromClass(ontology, cl, AnnotationVocabulary.hasRelatedSynonym));
		currentEntry.setSuperClassURIs(setSuperClassURIs (ontology, cl));
		currentEntry.setSubClassURIs(setSubClassURIs(ontology, cl));
		currentEntry.setSourceOntologyURL (getOntologyURL (ontology.getOntologyID().getOntologyIRI()));
		currentEntry.setSourceOntologyName (OntologyExtractor.getInstance().getOntologyName (ontology.getOntologyID().getOntologyIRI()));
		
		return currentEntry;
	}
	
	private List <String> getAnnotationPropertyFromClass (OWLOntology ontology, OWLClass cl, OWLAnnotationProperty annotationProperty) {
		List <String> annotationPropertyValues = new ArrayList <String> ();

		Set<OWLAnnotation> annotations = cl.getAnnotations(ontology, annotationProperty);

		if (annotations.size() == 0) {
			annotationPropertyValues.add(0, "-");
		}
		else {
			for(OWLAnnotation anno : annotations) {
				OWLAnnotationValue value = anno.getValue();
				annotationPropertyValues.add(((OWLLiteral)value).getLiteral());
			}
		}
		return annotationPropertyValues;
	}
	
	private List <String> setSubClassURIs(OWLOntology ontology, OWLClass cl) {
		List <String> subClasses = new ArrayList<String>();
		for (OWLClassExpression exp : cl.getSubClasses(ontology)) {
			if (exp.isClassExpressionLiteral()) {
				subClasses.add(exp.toString());
			}
		}
		return subClasses;
	}

	private List <String> setSuperClassURIs(OWLOntology ontology, OWLClass cl) {
		List <String> superClasses = new ArrayList<String>();
		for (OWLClassExpression exp : cl.getSuperClasses(ontology)) {
			if (exp.isClassExpressionLiteral()) {
				superClasses.add(exp.toString());
			}
		}
		return superClasses;
	}
	
	private String getOntologyURL(IRI ontologyIRI) {
		for (OBOFoundryOntology ontology : OBOFoundryOntology.values()) {
			String url = ontology.getURL(ontologyIRI.toString());
			if (url != null) {
				return url;
			}
		}
		throw new EncyclopaediaException("Could not find ontology URL among the stored ontologies " + ontologyIRI);
	}
}
