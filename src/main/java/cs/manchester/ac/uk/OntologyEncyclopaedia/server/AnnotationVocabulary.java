package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

public class AnnotationVocabulary {
	
	private static OWLDataFactory df = OntologyLoader.getDataFactory();
	
	public static final OWLAnnotationProperty hasDefinition = df.getOWLAnnotationProperty(IRI.create("http://purl.obolibrary.org/obo/IAO_0000115"));
	public static final OWLAnnotationProperty hasExactSynonym = df.getOWLAnnotationProperty(IRI.create("http://www.geneontology.org/formats/oboInOwl#hasExactSynonym"));
	public static final OWLAnnotationProperty hasBroadSynonym = df.getOWLAnnotationProperty(IRI.create("http://www.geneontology.org/formats/oboInOwl#hasBroadSynonym"));
	public static final OWLAnnotationProperty hasNarrowSynonym = df.getOWLAnnotationProperty(IRI.create("http://www.geneontology.org/formats/oboInOwl#hasNarrowSynonym"));
	public static final OWLAnnotationProperty hasRelatedSynonym = df.getOWLAnnotationProperty(IRI.create("http://www.geneontology.org/formats/oboInOwl#hasRelatedSynonym"));
	public static final OWLAnnotationProperty rdfsLabel = df.getOWLAnnotationProperty(OWLRDFVocabulary.RDFS_LABEL.getIRI());
	public static final OWLAnnotationProperty isDeprecated = df.getOWLAnnotationProperty(IRI.create("http://www.w3.org/2002/07/owl#deprecated"));
}
