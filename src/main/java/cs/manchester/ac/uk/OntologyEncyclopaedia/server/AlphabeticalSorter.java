package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


public class AlphabeticalSorter {

	private static List<EncyclopaediaEntry> listToSort = new ArrayList<EncyclopaediaEntry>();
	private ArrayList<List <EncyclopaediaEntry>> buckets;
	private static AlphabeticalSorter instance = null;

	private AlphabeticalSorter () {
		buckets = new ArrayList<List<EncyclopaediaEntry>>();
	}

	public static AlphabeticalSorter getInstance (Map <String, EncyclopaediaEntry> mapToSort) {
		if(instance == null) {
			instance = new AlphabeticalSorter();
		}
		listToSort.clear();
		listToSort = getMapEntries(mapToSort);
		return instance;
	}

	public static AlphabeticalSorter getInstance () {
		if(instance == null) {
			instance = new AlphabeticalSorter();
		}
		return instance;
	}

	private static List <EncyclopaediaEntry> getMapEntries (Map <String, EncyclopaediaEntry> map) {
		List <EncyclopaediaEntry> resultList = new ArrayList <EncyclopaediaEntry> ();
		for (String key : map.keySet()) {
			resultList.add(map.get(key));
		}

		return resultList;
	}

	public void sortIntoAlphabeticalBuckets () {
		if (buckets.isEmpty()) {
			for (char letter = 'a'; letter <= 'z'; letter++) {
				buckets.add(new ArrayList <EncyclopaediaEntry> ());
			}
		} else {
			for (List<EncyclopaediaEntry> item : buckets) {
				item.clear();
			}
		}
		for (int index = 0; index < listToSort.size(); index ++) {
			String term = listToSort.get(index).getName().toLowerCase();

			int firstAlphaCharacter = getFirstAlphaCharacter (term);
			if (firstAlphaCharacter >= 0) {
				List <EncyclopaediaEntry> currentBucket = buckets.get(firstAlphaCharacter);
				if (!currentBucket.contains(listToSort.get(index))) {
					currentBucket.add(listToSort.get(index));
				}
			}
		}
		sortBuckets();
	}

	private int getFirstAlphaCharacter(String term) {
		for (int i = 0; i < term.length(); i++) {
			char c = term.charAt(i);
			if (Character.isLetter(c)) {
				return c - 97;
			}

		}
		return -1;
	}

	public List <EncyclopaediaEntry> getBucketWithLetter (char letter) {
		if (buckets.size() != 0) {
			List<EncyclopaediaEntry> bucketToGet = buckets.get (letter - 97);
			if (!bucketToGet.isEmpty()) {
				return bucketToGet;
			}
		}
		return null;
	}

	private void sortBuckets () {
		for (int currentBucket = 0; currentBucket < buckets.size(); currentBucket ++) {
			Collections.sort(buckets.get(currentBucket), new Comparator<EncyclopaediaEntry>() {

				public int compare(EncyclopaediaEntry class1, EncyclopaediaEntry class2) {
					return class1.compareTo(class2);
				}
			});
		}
	}
}