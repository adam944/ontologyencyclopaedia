package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SuperAndSubclassMatcher {

	private SuperAndSubclassMatcher () {

	}

	public static void findSuperAndSubClasses(Map<String, EncyclopaediaEntry> classesWithProperties) {
		for (String key : classesWithProperties.keySet()) {
			EncyclopaediaEntry entry = classesWithProperties.get(key);

			List <SuperOrSubClass> superClasses = new ArrayList <SuperOrSubClass> ();
			List <SuperOrSubClass> subClasses = new ArrayList <SuperOrSubClass> ();
			
			for (String uri : entry.getSuperClassURIs()) {
				if (classesWithProperties.containsKey(uri)) {
					SuperOrSubClass superClass = new SuperOrSubClass(uri);
					superClass.setName(classesWithProperties.get(uri).getName());
					superClasses.add(superClass);
				}
			}

			for (String uri : entry.getSubClassURIs()) {
				if (classesWithProperties.containsKey(uri)) {
					SuperOrSubClass subClass = new SuperOrSubClass(uri);
					subClass.setName(classesWithProperties.get(uri).getName());
					subClasses.add(subClass);
				}
			}

			// TODO: sort map by values
//			Collections.sort(superClasses);
//			Collections.sort(subClasses);

			entry.setSuperClasses(superClasses);
			entry.setSubClasses(subClasses);
		}
	}
}
