package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class EncyclopaediaEntry implements Comparable<EncyclopaediaEntry> {
	private String URI, name, definition, tag, sourceOntologyURL, sourceOntologyName;
	private double rating;

	private List <String> exactSynonyms = new ArrayList <String> ();
	private List <String> broadSynonyms = new ArrayList <String> ();
	private List <String> narrowSynonyms = new ArrayList <String> ();
	private List <String> relatedSynonyms = new ArrayList <String> ();

	private List<String> superClassURIs = new ArrayList<String>();
	private List<String> subClassURIs = new ArrayList<String>();
	private List <SuperOrSubClass> superClasses = new ArrayList <SuperOrSubClass>();
	private List <SuperOrSubClass> subClasses = new ArrayList <SuperOrSubClass>();
	
	@SuppressWarnings("unused")
	private EncyclopaediaEntry () {
		
	}
	
	public EncyclopaediaEntry (String URI) {
		this.URI = URI;
	}

	public List<String> getExactSynonyms() {
		Collections.sort(exactSynonyms);
		return exactSynonyms;
	}

	public void setExactSynonyms(List<String> exactSynonyms) {
		this.exactSynonyms = exactSynonyms;
	}

	public List<String> getBroadSynonyms() {
		Collections.sort(broadSynonyms);
		return broadSynonyms;
	}

	public void setBroadSynonyms(List<String> broadSynonyms) {
		this.broadSynonyms = broadSynonyms;
	}

	public List<String> getNarrowSynonyms() {
		Collections.sort(narrowSynonyms);
		return narrowSynonyms;
	}

	public void setNarrowSynonyms(List<String> narrowSynonyms) {
		this.narrowSynonyms = narrowSynonyms;
	}

	public List<String> getRelatedSynonyms() {
		Collections.sort(relatedSynonyms);
		return relatedSynonyms;
	}

	public void setRelatedSynonyms(List<String> relatedSynonyms) {
		this.relatedSynonyms = relatedSynonyms;
	}

	public String getURI() {
		return URI;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name.trim();
		setTag(name.trim());
	}
	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public List<String> getSubClassURIs() {
		return subClassURIs;
	}

	public void setSubClassURIs(List <String> subClass) {
		this.subClassURIs = subClass;
	}

	public List <String> getSuperClassURIs() {
		return superClassURIs;
	}

	public void setSuperClassURIs(List <String> superClass) {
		this.superClassURIs = superClass;
	}

	public List <SuperOrSubClass> getSuperClasses() {
		return superClasses;
	}

	public void setSuperClasses(List <SuperOrSubClass> superClasses) {
		this.superClasses = superClasses;
	}

	public List <SuperOrSubClass> getSubClasses() {
		return subClasses;
	}

	public void setSubClasses(List <SuperOrSubClass> subClasses) {
		this.subClasses = subClasses;
	}

	public String getTag() {
		return tag;
	}

	private void setTag(String name) {
		this.tag = name.replace(" ", "-");
	}

	public void setSourceOntologyURL(String ontologyURL) {
		this.sourceOntologyURL = ontologyURL;
	}

	public void setSourceOntologyName(String ontologyName) {
		this.sourceOntologyName = ontologyName;
	}

	public String getSourceOntologyName () {
		return sourceOntologyName;
	}

	public String getSourceOntologyURL () {
		return sourceOntologyURL;
	}

	public char getStartingLetter () {
		for (int i = 0; i < name.length(); i++) {
			char c = name.charAt(i);
			if (Character.isLetter(c)) {
				return c;
			}
		}
		throw new EncyclopaediaException("Encyclopaedia entry " + name + "does not have alphabetical characters in its name.");
	}

	public int compareTo (EncyclopaediaEntry otherClass) {
		return this.getName().toLowerCase().compareTo(otherClass.getName().toLowerCase());
	}
	
	public void setRating (double rating) {
		this.rating = rating;
	}
	
	public double getRating () {
		return rating;
	}
}
