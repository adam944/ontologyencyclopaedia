package cs.manchester.ac.uk.OntologyEncyclopaedia.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.BinaryRequestWriter;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocumentList;

import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaEntry;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyExtractor;


public class SearchEngineImpl {

	private static SearchEngineImpl instance = null;
	Map <String, EncyclopaediaEntry> classMap = OntologyExtractor.getInstance().getClassesWithProperties(OntologyExtractor.getInstance().getNonObsoleteClasses());
	private HttpSolrServer server = new HttpSolrServer("http://localhost:8983/solr/encyclopaedia/");

	private SearchEngineImpl () {

	}

	public static SearchEngineImpl getInstance () {
		if (instance == null) {
			instance = new SearchEngineImpl();
		}

		return instance;
	}

	public List<EncyclopaediaEntry> searchTerm (String fieldName, String term) {
		server.setParser(new XMLResponseParser());
		server.setRequestWriter(new BinaryRequestWriter());

		List <EncyclopaediaEntry> results = new ArrayList <EncyclopaediaEntry> ();

		SolrDocumentList resDocs = performQuery(fieldName, term);

		for (int i = 0; i < resDocs.size(); i++) {
			results.add(classMap.get(resDocs.get(i).getFieldValue("uri")));
		}

		return results;
	}

	private SolrDocumentList performQuery(String fieldName, String term) {
		SolrDocumentList resDocs = new SolrDocumentList();
		if (term != "") {
			SolrQuery query = new SolrQuery();
			query.setQuery(constructQueryString (fieldName, term));
			query.setRows(100);
			QueryResponse rsp = null;
			try {
				rsp = server.query( query );
			} catch (SolrServerException e) {
				System.err.println("The Solr engine could not be queried.");
				e.printStackTrace();
			}
			resDocs = rsp.getResults();
		}
		return resDocs;
	}

	private String constructQueryString(String field, String term) {
		return field + ":" + ClientUtils.escapeQueryChars(term);
	}
}
