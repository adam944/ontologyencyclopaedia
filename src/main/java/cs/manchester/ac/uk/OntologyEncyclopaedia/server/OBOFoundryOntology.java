package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

public enum OBOFoundryOntology {
	GO ("http://purl.obolibrary.org/obo/go.owl", "go.owl", "http://geneontology.org/", "Gene Ontology"), 
	PR ("http://purl.obolibrary.org/obo/pr.owl", "pr.owl", "http://pir.georgetown.edu/pro/pro.shtml", "Protein Ontology"),
	ZFA ("http://purl.obolibrary.org/obo/zfa.owl","zfa.owl", "http://zfin.org/zf_info/anatomy/dict/sum.html", "Zebrafish anatomy and development"), 
	OBI ("http://purl.obolibrary.org/obo/obi.owl", "obi.owl", "http://purl.obofoundry.org/obo/obi", "Ontology for biomedical investigations"),
	XAO ( "http://purl.obolibrary.org/obo/xao.owl", "xao.owl", "http://www.xenbase.org/anatomy/xao.do?method=display", "Xenopus anatomy and development"),
	DOID ("http://purl.obolibrary.org/obo/doid.owl", "doid.owl", "http://diseaseontology.sourceforge.net/", "Human disease ontology"),
	HP ("http://purl.obolibrary.org/obo/hp.owl", "hp.owl", "http://www.human-phenotype-ontology.org/", "Human phenotype ontology"), 
	CHEBI ("http://purl.obolibrary.org/obo/chebi.owl", "chebi.owl", "http://www.ebi.ac.uk/chebi", "Chemical entities of biological interest"), 
	PATO ("http://purl.obolibrary.org/obo/pato.owl", "pato.owl", "http://wiki.obofoundry.org/wiki/index.php/PATO:Main_Page", "Phenotypic Quality"), 
	PO ("http://purl.obolibrary.org/obo/po.owl", "po.owl", "http://www.plantontology.org", "Plant Ontology");
	
	private String iri;
	private String url;
	private String name;
	private String filename;
	
	private OBOFoundryOntology (String iri, String filename, String url, String name) {
		this.iri = iri;
		this.filename = filename;
		this.url = url;
		this.name = name;
	}
	
	public String getURL (String iri) {
		if (iri.equals(this.iri))
			return url;
		return null;
	}
	
	public String getName (String iri) {
		if (iri.equals(this.iri))
			return name;
		return null;
	}
	
	public String getIri() {
		return iri;
	}
	
	public String getFilename(String iri) {
		if (iri.equals(this.iri))
			return filename;
		return null;
	}
}
