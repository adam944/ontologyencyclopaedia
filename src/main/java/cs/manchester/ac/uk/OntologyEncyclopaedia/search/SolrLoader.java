package cs.manchester.ac.uk.OntologyEncyclopaedia.search;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.common.SolrInputDocument;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;

import com.google.common.collect.Multimap;

import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaEntry;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyExtractor;


public class SolrLoader {

	static HttpSolrServer server = new HttpSolrServer("http://localhost:8983/solr/encyclopaedia/");

	public static void deleteAllEntires () {

		server.setParser(new XMLResponseParser());

		try {
			server.deleteByQuery("*:*");
			server.commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Deleted all entries from the Solr engine");
	}

	public static void deleteOntology (String iri) {
		String ontologyName = iri.substring(iri.indexOf("obo/") + 4, iri.indexOf(".owl"));
		String wildcarderUri = "<http\\://purl.obolibrary.org/obo/" + ontologyName.toUpperCase() + "*>";

		server.setParser(new XMLResponseParser());

		try {
			server.deleteByQuery("uri:" + wildcarderUri);
			server.commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void addOntology (File ontologyFile) {
		server.setParser(new XMLResponseParser());

		OntologyExtractor extractor = OntologyExtractor.getInstance();
		extractor.initialise();

		Multimap <OWLOntology, OWLClass> nonObsoleteClasses = extractor.getNonObsoleteClasses();
		Map <String, EncyclopaediaEntry> encyclopaediaClasses =  extractor.getClassesWithProperties(nonObsoleteClasses);
		int index = 1;
		Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		for (String key : encyclopaediaClasses.keySet()) {
			EncyclopaediaEntry entry = encyclopaediaClasses.get(key);
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", index);
			doc.addField("className", entry.getName());
			doc.addField("uri", entry.getURI());
			docs.add( doc );
			index++;
		}

		System.out.println("Added " + index + " classes to the Solr engine");

		try {
			server.add(docs);
			server.commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
