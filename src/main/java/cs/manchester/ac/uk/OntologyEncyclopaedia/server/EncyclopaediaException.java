package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

public class EncyclopaediaException extends RuntimeException {
	
	private static final long serialVersionUID = -3942771922655492442L;

	public EncyclopaediaException (String message) {
		super (message);
	}
}
