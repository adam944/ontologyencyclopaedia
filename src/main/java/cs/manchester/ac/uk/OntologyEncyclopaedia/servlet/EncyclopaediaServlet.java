package cs.manchester.ac.uk.OntologyEncyclopaedia.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs.manchester.ac.uk.OntologyEncyclopaedia.server.AlphabeticalSorter;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaEntry;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaException;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.db.DatabaseReader;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.db.DatabaseWriter;

public class EncyclopaediaServlet extends HttpServlet {

	private static final long serialVersionUID = -5517477050681351175L;
	private final int INITIAL_NUMBER_OF_CLASSES_TO_BE_LOADED = 50;
	private int startingIndex, endingIndex = 0;
	private char lastReqLetter;

	public EncyclopaediaServlet() {
		super();
	}

	protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		if (request.getParameter("letter") != null) {
			char reqLetter = request.getParameter("letter").toLowerCase().charAt(0);
			lastReqLetter = reqLetter;
			List<EncyclopaediaEntry> allSortedClassesForLetter = getAllSortedClasses(reqLetter);
			if (allSortedClassesForLetter != null) {
				if (Character.isLetter(reqLetter)) {
					if (request.getParameter("uri") != null) {
						startingIndex = getIndexByURI(request.getParameter("uri"), allSortedClassesForLetter);
						List<EncyclopaediaEntry> classesToDisplay = getClassesToDisplay(allSortedClassesForLetter, startingIndex, -1);
						request.setAttribute("classesWithProperties", classesToDisplay);
						request.setAttribute("startingAndEndingValues", getStartingAndEndingValues (classesToDisplay));
					} else {
						startingIndex = 0;
						List<EncyclopaediaEntry> classesToDisplay = getClassesToDisplay(allSortedClassesForLetter, 0, -1);
						request.setAttribute("classesWithProperties", classesToDisplay);
						request.setAttribute("startingAndEndingValues", getStartingAndEndingValues (classesToDisplay));
					}
				} else {
					throw new EncyclopaediaException("Request to servlet failed: request parameter " + reqLetter + " is not a valid letter.");
				}

				request.setAttribute(reqLetter + "_" + "active", "menu_active");
				rd = getServletContext().getRequestDispatcher("/byLetter.jsp");

			} else {
				rd = getServletContext().getRequestDispatcher("/noEntries.jsp");
			}
			endingIndex = startingIndex + INITIAL_NUMBER_OF_CLASSES_TO_BE_LOADED;
			rd.forward(request, response);
		}

		if (request.getParameter("loadMore") != null) {
			int previousStartingIndex = startingIndex;
			int previousEndingIndex = endingIndex;
			if (request.getParameter("loadMore").equals("top")) {
			startingIndex -= INITIAL_NUMBER_OF_CLASSES_TO_BE_LOADED;
			} else if (request.getParameter("loadMore").equals("bottom")) {
				endingIndex += INITIAL_NUMBER_OF_CLASSES_TO_BE_LOADED;
			}
			request.setAttribute(lastReqLetter + "_" + "active", "menu_active");
			request.setAttribute("startingAndEndingValues", getStartingAndEndingValues (getClassesToDisplay(getAllSortedClasses(lastReqLetter), previousStartingIndex, previousEndingIndex)));
			request.setAttribute("classesWithProperties", getClassesToDisplay(getAllSortedClasses(lastReqLetter), startingIndex, endingIndex));
			rd = getServletContext().getRequestDispatcher("/byLetter.jsp");
			rd.forward(request, response);
		}
	}

	protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseWriter writer = new DatabaseWriter();
		writer.addNewRating(request.getParameter("uri"), Integer.parseInt(request.getParameter("rating")));
		
		DatabaseReader reader = new DatabaseReader();
		response.setContentType("text/plain");  
	    response.setCharacterEncoding("UTF-8"); 
	    response.getWriter().write(reader.getAverageRatingForUri(request.getParameter("uri")).toString());   
	}

	private List<EncyclopaediaEntry> getAllSortedClasses (char reqLetter) {
		if (AlphabeticalSorter.getInstance().getBucketWithLetter(reqLetter) != null) {
			return AlphabeticalSorter.getInstance().getBucketWithLetter(reqLetter);
		} 
		return null;
	}

	private List <EncyclopaediaEntry> getClassesToDisplay (List <EncyclopaediaEntry> entryList, int startingNumber, int endingNumber) {

		int toStartWith = startingNumber;
		int toEndWith;
		if (endingNumber == -1) {
			toEndWith = startingNumber + INITIAL_NUMBER_OF_CLASSES_TO_BE_LOADED;
		} else {
			toEndWith = endingNumber; 
		}
		if (toStartWith - INITIAL_NUMBER_OF_CLASSES_TO_BE_LOADED < 0) {
			toStartWith = 0;
		}
		else {
			toStartWith = toStartWith - INITIAL_NUMBER_OF_CLASSES_TO_BE_LOADED;
		}

		if (toEndWith > entryList.size()) {
			toEndWith = entryList.size() - 1;
		}
		
		if (endingIndex <= toEndWith) {
			endingIndex = toEndWith;
		}
		
		List<EncyclopaediaEntry> classesToDisplay = entryList.subList(toStartWith, toEndWith);
		classesToDisplay = getRatingForEntries(classesToDisplay);
		return classesToDisplay;
	}

	private int getIndexByURI(String uri, List<EncyclopaediaEntry> list) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) !=null && list.get(i).getURI().equals(uri)) {
				return i;
			}
		}
		throw new EncyclopaediaException("The URI " + uri + " does not match with any entry.");
	}
	
	private String[] getStartingAndEndingValues(List<EncyclopaediaEntry> classesToDisplay) {
		String[] startEnd = new String[2];
		startEnd[0] = classesToDisplay.get(0).getTag();
		startEnd[1] = classesToDisplay.get(classesToDisplay.size() - 1).getTag();
		return startEnd;
	}
	
	private List<EncyclopaediaEntry> getRatingForEntries (List<EncyclopaediaEntry> entryList) {
		DatabaseReader reader = new DatabaseReader();
		for (EncyclopaediaEntry entry : entryList) {
			entry.setRating(reader.getAverageRatingForUri(entry.getURI()));
		}
		return entryList;
	}

}
