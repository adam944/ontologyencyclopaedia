package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * 
 * @author Adam Nogradi
 * Ontology Extractor Singleton
 *
 */
public class OntologyExtractor {

	private Map <String, OWLOntology> ontologies;
	private Map <String, EncyclopaediaEntry> classMap = new HashMap<String, EncyclopaediaEntry> ();

	private static OntologyExtractor instance = null;

	private OntologyExtractor () {

	}

	public static OntologyExtractor getInstance () {
		if(instance == null) {
			instance = new OntologyExtractor();
		}
		return instance;
	}

	public void initialise () {
		ontologies = OntologyLoader.getOntologies();
	}

	public Multimap <OWLOntology, OWLClass> getNonObsoleteClasses() {

		Multimap <OWLOntology, OWLClass> nonObsoleteMap = ArrayListMultimap.create();
		for (String iri : ontologies.keySet()) {
			OWLOntology ontology = ontologies.get(iri);
			for (OWLClass cls : ontology.getClassesInSignature()) {
				Set<OWLAnnotation> annotations = cls.getAnnotations(ontology, AnnotationVocabulary.isDeprecated);
				if (annotations.size() == 0) {
					nonObsoleteMap.put(ontology, cls);
				}
			}
		}
		return nonObsoleteMap;
	}

	public Map<String, EncyclopaediaEntry> getClassesWithProperties (Multimap<OWLOntology, OWLClass> classes) throws NullPointerException{
		EncyclopaediaEntryFactory entryFactory = new EncyclopaediaEntryFactory();
		for(Map.Entry<OWLOntology, OWLClass> entry : classes.entries()) {
			OWLClass cl = entry.getValue();
			OWLOntology ontology = entry.getKey();
			if (!classMap.containsKey(cl.toString())) {
				EncyclopaediaEntry currentEntry = entryFactory.getNewEntry(ontology, cl);
				classMap.put(cl.toString(), currentEntry);
			}
		}
		return classMap;
	}

	public String getOntologyName(IRI ontologyIRI) {
		for (OBOFoundryOntology ontology : OBOFoundryOntology.values()) {
			String name = ontology.getName(ontologyIRI.toString());
			if (name != null) {
				return name;
			}
		}
		throw new EncyclopaediaException("Could not find ontology name among the stored ontologies+ " + ontologyIRI);
	}
	
	public void clearClassMap () {
		classMap.clear();
	}
}
