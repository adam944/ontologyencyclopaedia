package cs.manchester.ac.uk.OntologyEncyclopaedia.server.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DatabaseWriter extends DatabaseConnector{

	private void performWrite (String sql) {
		super.connect();
		Connection connection = super.connection;
		Statement stmt = null;

		try {
			stmt = connection.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addNewRating (String uri, int rating) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String currentDate = dateFormat.format(cal.getTime());
		String currentTime = timeFormat.format(cal.getTime());

		String sql = "INSERT INTO RATINGS (URI,RATING,DATE,TIME) " + "VALUES ('" + uri + "'," +  rating + ",'" + currentDate + "','" + currentTime + "' );";
		performWrite(sql);

	}

}
