package cs.manchester.ac.uk.OntologyEncyclopaedia.servlet;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs.manchester.ac.uk.OntologyEncyclopaedia.search.SearchEngineImpl;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaEntry;

public class SearchServlet extends HttpServlet{

	private static final long serialVersionUID = 610371601415134368L;

	protected void doGet (HttpServletRequest request, HttpServletResponse response) {
		
	}

	protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List <EncyclopaediaEntry> results = SearchEngineImpl.getInstance().searchTerm("className", request.getParameter("term"));
		request.setAttribute("term", request.getParameter("term"));
		if (results.isEmpty()) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/search_noresults.jsp");
			rd.forward(request, response);
		} else {
			request.setAttribute("results", results);
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/search.jsp");
			rd.forward(request, response);
		}
	}

}
