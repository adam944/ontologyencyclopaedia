package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

public class SuperOrSubClass {
	private String uri, name;
	
	@SuppressWarnings("unused")
	private SuperOrSubClass () {
		
	}
	
	public SuperOrSubClass (String uri) {
		this.uri = uri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.trim();
	}
	
	public char getStartingLetter () {
		for (int i = 0; i < name.length(); i++) {
			char c = name.charAt(i);
			if (Character.isLetter(c)) {
				return c;
			}
		}
		throw new EncyclopaediaException("Encyclopaedia entry " + name + " (URI: " + uri + " )" + " does not have alphabetical characters in its name.");
	}
}
