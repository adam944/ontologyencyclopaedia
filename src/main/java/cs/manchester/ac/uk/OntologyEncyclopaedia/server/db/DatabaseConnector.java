package cs.manchester.ac.uk.OntologyEncyclopaedia.server.db;

import java.sql.*;

public abstract class DatabaseConnector {
	
	protected Connection connection = null;
	
	protected void connect () {
		
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:/var/triple/obopedia/db/ratings.db");
//			connection = DriverManager.getConnection("jdbc:sqlite:/Users/AdamNogradi/Google Drive/3rd year project/DB/ratings.db");
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
}
