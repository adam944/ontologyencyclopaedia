package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public class OntologyLoader {

	private static Map <String, OWLOntology> ontologyMap = new HashMap <String, OWLOntology> ();
	private static OWLDataFactory df;
	private OWLOntologyManager man;
	private static int numberOfOntologies = 0;
	private static OntologyLoader instance = null;

	private OntologyLoader () {
		man = OWLManager.createOWLOntologyManager();
		df = man.getOWLDataFactory();
	}

	public static OntologyLoader getInstance () {
		if (instance == null) {
			instance = new OntologyLoader();
		}
		return instance;
	}


	public void loadOntology (File ontologyFile) {
		OWLOntology	ontology = null;
		try {
			ontology = man.loadOntologyFromOntologyDocument(ontologyFile);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		ontologyMap.put(ontology.getOntologyID().getOntologyIRI().toString(), ontology);
		numberOfOntologies++;
		System.out.println("Loaded: " + ontology.getOntologyID().getOntologyIRI() + " Version " + ontology.getOntologyID().getVersionIRI());
	}

	public static Map <String, OWLOntology> getOntologies () {
		return ontologyMap;
	}

	public static OWLDataFactory getDataFactory () {
		return df;
	}

	public static int getNumberOfOntologies () {
		return numberOfOntologies;
	}

	public void removeAllOntologies () {
		numberOfOntologies = 0;
		for (String key : ontologyMap.keySet()) {
			OWLOntology ontology = ontologyMap.get(key);
			man.removeOntology(ontology);
		}

		ontologyMap.clear();
	}

	public void removeOntology (String ontologyIRIString) {
		OWLOntology ontology = ontologyMap.get(ontologyIRIString);

		Set<String> set = ontologyMap.keySet();
		Iterator<String> itr = set.iterator();
		while (itr.hasNext())
		{
			String nextOntology = itr.next();
			if (nextOntology.equals(ontologyIRIString)) {
				ontologyMap.remove(nextOntology);
				man.removeOntology(ontology);
				for (OWLOntology ont : man.getOntologies()) {
					System.out.println(ont);
				}
				numberOfOntologies--;
			}
		}
	}
}
