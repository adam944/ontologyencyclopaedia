package cs.manchester.ac.uk.OntologyEncyclopaedia.server.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DatabaseReader extends DatabaseConnector {

	private ResultSet performSelect (String query) {

		super.connect();
		Connection connection = super.connection;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	public Double getAverageRatingForUri (String uri) {
		String queryString = "SELECT RATING FROM RATINGS WHERE URI='" + uri + "';";

		List <Integer> ratingsForTerm = new ArrayList <Integer> ();
		ResultSet rs = performSelect(queryString);
		try {
			while (rs.next()) {
				ratingsForTerm.add(rs.getInt("rating"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		int sum = 0;
		for (Integer i : ratingsForTerm) {
			sum+=i;
		}
		Double average = (double)sum / (double)ratingsForTerm.size();
		if (!average.isNaN()) {
			DecimalFormat formatter = new DecimalFormat("#.#");
			return Double.parseDouble(formatter.format(average));
		} else {
			return 0.0;
		}
	}
}
