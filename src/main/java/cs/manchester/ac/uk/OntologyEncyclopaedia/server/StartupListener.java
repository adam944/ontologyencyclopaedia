package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.semanticweb.owlapi.model.IRI;

import cs.manchester.ac.uk.OntologyEncyclopaedia.search.SolrLoader;
import cs.manchester.ac.uk.OntologyEncyclopaedia.servlet.AdminServlet;
import cs.manchester.ac.uk.OntologyEncyclopaedia.statistics.Metrics;

public class StartupListener implements ServletContextListener {
	

	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("***** Initialising the Encyclopaedia... *****");
		
		addAvailableOntologies();
		Metrics.initialise();
		SolrLoader.deleteAllEntires();
	}
	
	private void addAvailableOntologies () {
		for (OBOFoundryOntology ontology : OBOFoundryOntology.values()) {
			String iriString = ontology.getIri();
			
			OntologyOption newOption = new OntologyOption(iriString);
			IRI iri = IRI.create(iriString);
			newOption.setName(OntologyExtractor.getInstance().getOntologyName(iri));
			AdminServlet.availableOntologies.add(newOption);
		}
	}
	
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

}
