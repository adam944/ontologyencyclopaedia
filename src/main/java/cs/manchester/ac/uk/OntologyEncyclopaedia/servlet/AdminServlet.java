package cs.manchester.ac.uk.OntologyEncyclopaedia.servlet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import cs.manchester.ac.uk.OntologyEncyclopaedia.search.SolrLoader;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.AlphabeticalSorter;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaEntry;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaException;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OBOFoundryOntology;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyExtractor;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyLoader;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyOption;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.SuperAndSubclassMatcher;
import cs.manchester.ac.uk.OntologyEncyclopaedia.statistics.Metrics;

public class AdminServlet extends HttpServlet{

	private static final long serialVersionUID = -902182209964453974L;

	public static List <OntologyOption> availableOntologies = new ArrayList <OntologyOption> ();
	//private File filesLocation = new File ("/var/triple/obopedia/owl/");
	private File filesLocation = new File ("/Users/AdamNogradi/Google Drive/3rd year project/OBO Foundry/");

	public AdminServlet () {
		super();
	}

	protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");
		String pageToLoad = request.getParameter("page");
		String action = request.getParameter("action");
		RequestDispatcher rd = null;

		Subject currentUser = SecurityUtils.getSubject();

		if (!currentUser.isAuthenticated() ) {
			rd = getServletContext().getRequestDispatcher("/login.jsp");
		}
		else if (currentUser.isAuthenticated() && pageToLoad == null) {
			setStatsAttributes(request);
			rd = getServletContext().getRequestDispatcher("/admin/");
		} else if (currentUser.isAuthenticated()){
			request.setAttribute("availableOntologies", getAvailableOntologies ());
			rd = getServletContext().getRequestDispatcher("/admin/" + pageToLoad + ".jsp");
		}
		if (action != null && action.equals("addRemove")) {
			if (request.getParameter("add") != null) {
				String ontologyToAdd = request.getParameter("add");
				File ontologyFileName = getOntologyFileName (ontologyToAdd);
				OntologyLoader.getInstance().loadOntology(ontologyFileName);
				changeOptionFlag (ontologyToAdd, true);
				reloadSystem();
				SolrLoader.addOntology(ontologyFileName);
			}

			else if (request.getParameter("remove") != null) {
				String ontologyToRemove = request.getParameter("remove");
				OntologyLoader.getInstance().removeOntology(ontologyToRemove);
				OntologyExtractor.getInstance().clearClassMap();
				changeOptionFlag (ontologyToRemove, false);
				reloadSystem();
				SolrLoader.deleteOntology(ontologyToRemove);
			}
		}
		setStatsAttributes(request);
		rd.forward(request, response);
	}

	protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");

		if (action.equals("login")) {
			// Set standard HTTP/1.1 no-cache headers.
			response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
			// Set standard HTTP/1.0 no-cache header.
			response.setHeader("Pragma", "no-cache");

			setStatsAttributes(request);

			Subject currentUser = SecurityUtils.getSubject();

			if (!currentUser.isAuthenticated() ) {
				UsernamePasswordToken token = new UsernamePasswordToken(request.getParameter("username"), request.getParameter("password"));
				token.setRememberMe(true);
				currentUser.login(token);
			}
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/admin/");
			rd.forward(request, response);
		}
	}

	private HttpServletRequest setStatsAttributes (HttpServletRequest request) {
		request.setAttribute("noOfOntologies", Metrics.getNumberOfOntologies());
		request.setAttribute("noOfClasses", Metrics.getTotalNumberOfClasses());
		request.setAttribute("averageSynonymsPerEntry", Metrics.getAvergeNumberOfSynonyms());
		request.setAttribute("obsoleteClasses", Metrics.getNumberOfObsoleteClasses());

		return request;
	}

	private List <OntologyOption> getAvailableOntologies() {
		return availableOntologies;
	}
	
	private File getOntologyFileName(String ontologyIRI) {
		for (OBOFoundryOntology ontology : OBOFoundryOntology.values()) {
			String filename = ontology.getFilename(ontologyIRI.toString());
			if (filename != null) {
				return new File (filesLocation + "/" + filename);
			}
		}
		throw new EncyclopaediaException("Could not find ontology filename among the stored ontologies+ " + ontologyIRI);
	}
	
	private void reloadSystem () {
		OntologyExtractor.getInstance().initialise();
		Map<String, EncyclopaediaEntry> classesWithProperties = OntologyExtractor.getInstance().getClassesWithProperties(OntologyExtractor.getInstance().getNonObsoleteClasses());
		AlphabeticalSorter sorter = AlphabeticalSorter.getInstance(classesWithProperties);
		sorter.sortIntoAlphabeticalBuckets();
		SuperAndSubclassMatcher.findSuperAndSubClasses(classesWithProperties);
		Metrics.initialise();
	}
	
	private void changeOptionFlag(String iri, boolean flag) {
		List<OntologyOption> options = getAvailableOntologies();
		for (OntologyOption option : options) {
			if (option.getIri().equals(iri)) {
				option.setHasBeenAdded(flag);
			}
		}
	}

}
