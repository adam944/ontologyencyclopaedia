package cs.manchester.ac.uk.OntologyEncyclopaedia.server;

public class OntologyOption {
	private String name, iri;
	private boolean hasBeenAdded = false;
	
	@SuppressWarnings("unused")
	private OntologyOption () {
		
	}
	
	public OntologyOption (String iri) {
		this.iri = iri;
	}
	
	public String getIri() {
		return iri;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public boolean hasBeenAdded() {
		return hasBeenAdded;
	}

	public void setHasBeenAdded(boolean hasBeenAdded) {
		this.hasBeenAdded = hasBeenAdded;
	}
	
	
}
