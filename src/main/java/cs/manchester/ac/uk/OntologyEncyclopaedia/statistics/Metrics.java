package cs.manchester.ac.uk.OntologyEncyclopaedia.statistics;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;

import com.google.common.collect.Multimap;

import cs.manchester.ac.uk.OntologyEncyclopaedia.server.EncyclopaediaEntry;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyExtractor;
import cs.manchester.ac.uk.OntologyEncyclopaedia.server.OntologyLoader;

public class Metrics {

	private static Map <String, EncyclopaediaEntry> classMap;

	private static List <Integer> noOfExactSynonyms = new ArrayList <Integer> ();
	private static List <Integer> noOfBroadSynonyms = new ArrayList <Integer> ();
	private static List <Integer> noOfNarrowSynonyms = new ArrayList <Integer> ();
	private static List <Integer> noOfRelatedSynonyms = new ArrayList <Integer> ();

	private static int nonObsoleteSize;

	private Metrics () {

	}

	public static void initialise () {
		OntologyExtractor.getInstance().initialise();
		Multimap<OWLOntology, OWLClass> nonObsoleteClasses = OntologyExtractor.getInstance().getNonObsoleteClasses();
		nonObsoleteSize = nonObsoleteClasses.size();
		classMap = OntologyExtractor.getInstance().getClassesWithProperties(nonObsoleteClasses);
	}

	public static double getAvergeNumberOfSynonyms () {

		if (classMap.size() != 0) {
			getNumbersOfSynonyms();

			List <Double> meansList = new ArrayList <Double> ();
			double exactMean = getMean(noOfExactSynonyms);
			meansList.add(exactMean);
			double narrowMean = getMean(noOfNarrowSynonyms);
			meansList.add(narrowMean);
			double broadMean = getMean(noOfBroadSynonyms);
			meansList.add(broadMean);
			double relatedMean = getMean(noOfRelatedSynonyms);
			meansList.add(relatedMean);

			double sum = 0;

			for (Double mean : meansList) {
				if (mean.isNaN()) {
					mean = 0.0;
				}

				sum += mean;
			}
			DecimalFormat formatter = new DecimalFormat("#.##");

			return Double.parseDouble(formatter.format(sum / 4));
		}
		return 0.0;
	}

	public static int getTotalNumberOfClasses () {
		Map <String, OWLOntology> ontologies = OntologyLoader.getOntologies();
		int total = 0;
		for (String key : ontologies.keySet()) {
			OWLOntology ont = ontologies.get(key);
			total += ont.getClassesInSignature().size();
		}
		return total;
	}

	public static int getNumberOfObsoleteClasses () {
		return getTotalNumberOfClasses() - nonObsoleteSize;
	}

	private static void getNumbersOfSynonyms() {
		for (String key : classMap.keySet()) {

			EncyclopaediaEntry entry = classMap.get(key);

			if (entry.getExactSynonyms().get(0) != "-") {
				noOfExactSynonyms.add(entry.getExactSynonyms().size());
			}
			if (entry.getBroadSynonyms().get(0) != "-") {
				noOfBroadSynonyms.add(entry.getBroadSynonyms().size());
			}
			if (entry.getNarrowSynonyms().get(0) != "-") {
				noOfNarrowSynonyms.add(entry.getNarrowSynonyms().size());
			}
			if (entry.getRelatedSynonyms().get(0) != "-") {
				noOfRelatedSynonyms.add(entry.getRelatedSynonyms().size());
			}
		}
	}

	private static double getMean (List <Integer> inputList) {
		double sum = 0;

		for (Integer number : inputList) {
			sum += number;
		}

		return sum / inputList.size();

	}

	public static int getNumberOfOntologies () {
		return OntologyLoader.getNumberOfOntologies();
	}
}
